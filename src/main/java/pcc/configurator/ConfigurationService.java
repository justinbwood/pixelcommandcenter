package pcc.configurator;

import java.util.ServiceLoader;

import pcc.configurator.spi.ConfigurationHandler;

public class ConfigurationService {
	private static ConfigurationService service;
	private ServiceLoader<ConfigurationHandler> loader;
	
	private String defaultConfigurator;
	
	private ConfigurationService() {
		loader = ServiceLoader.load(ConfigurationHandler.class);
		defaultConfigurator = "";
	}
	
	public static synchronized ConfigurationService getInstance() {
		if (service == null) {
			service = new ConfigurationService();
		}
		return service;
	}
	
	public ConfigurationHandler getDefaultService() throws Exception {
		for (ConfigurationHandler c : loader) {
			if (c.getServiceName().equals(defaultConfigurator)) {
				return c;
			}
		}
		throw new Exception("No default configurator is registered.");
	}
}
