package pcc.configurator.spi;

import java.util.HashMap;

import pcc.plugin.spi.PluginHandler;

public interface ConfigurationHandler extends PluginHandler {
	
	/**
	 * Gets a {@link HashMap} of the plugin's entire configuration.
	 * @return
	 */
	public HashMap<String, String> getPluginConfiguration();
	
	/**
	 * Gets the value of a specific plugin's key.
	 */
	public String getConfigOption();
	
	/**
	 * Sets a plugin's entire configuration.
	 */
	public void setPluginConfiguration();
	
	/**
	 * Sets the value of a specific plugin's key.
	 */
	public void setConfigOption();
}
