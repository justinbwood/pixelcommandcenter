package pcc.util;

public class Pair<A, B> {
	private final A first;
	private final B second;
	
	public Pair(A f, B s) {
		super();
		this.first = f;
		this.second = s;
	}
	
	public int hashCode() {
		int hashFirst = first != null ? first.hashCode() : 0;
    	int hashSecond = second != null ? second.hashCode() : 0;
    	return (hashFirst + hashSecond) * hashSecond + hashFirst;
	}
	
	public boolean equals(Object other) {
		if (other == null) return false;
		if (this == other) return true;
		if (!(other instanceof Pair)) return false;
		
		Pair otherPair = (Pair) other;
		return ((this.first == otherPair.first || 
					(this.first != null && otherPair.first != null &&
					this.first.equals(otherPair.first))) &&
				((this.second == otherPair.second ||
					(this.second != null && otherPair.second != null &&
					this.second.equals(otherPair.second)))));
	}
	
	public String toString() {
		return "(" + first + ", " + second + ")";
	}
}
