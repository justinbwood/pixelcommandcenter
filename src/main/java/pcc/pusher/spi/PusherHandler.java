package pcc.pusher.spi;

import pcc.plugin.spi.PluginHandler;

public interface PusherHandler extends PluginHandler {
	
	/**
	 * Start pushing pixels.
	 */
	public void start();
	
	/**
	 * Stop pushing pixels.
	 */
	public void stop();
	
}
