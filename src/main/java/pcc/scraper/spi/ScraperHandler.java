package pcc.scraper.spi;

import java.awt.Color;
import java.util.HashMap;

import pcc.plugin.spi.PluginHandler;
import pcc.util.Pair;

public interface ScraperHandler extends PluginHandler {
	
	/**
	 * Returns a {@link HashMap} of String key value pairs representing
	 * the configuration of the scraper and device.
	 * @return				the configuration of the service
	 */
	public HashMap<String, String> getConfiguration();
	
	/**
	 * Sets configuration options of the scraper and device using
	 * the supplied {@link HashMap}
	 * <p>
	 * The supplied configuration does not have to be complete;
	 * the method will iterate over the provided keys and update
	 * their respective values.
	 * @param configMap		a map of config options to update
	 */
	public void setConfiguration(HashMap<String, String> configMap);
	
	/**
	 * Gets the virtual dimensions of the scraper. The first value
	 * represents the height, the second the width.
	 * <p>
	 * The design details are left to the implementation to allow
	 * for things such as zig-zag displays, abstract shapes, and
	 * other forms of non-contiguous displays.
	 * @return				a pair of integers representing height and width
	 */
	public Pair<Integer, Integer> getDimensions();
	
	/**
	 * Selects a color mode available to the device, and returns a boolean
	 * representing whether or not that color mode was valid.
	 * @param mode			the desired color mode
	 * @return				<b>true</b> if the color mode was available
	 */
	public boolean setColorMode(String mode);
	
	/**
	 * Gets an integer representing the maximum fps at the given
	 * display dimensions.
	 * <p>
	 * The design details are left to the implementation to allow
	 * for things such as zig-zag displays, abstract shapes, and
	 * other forms of non-contiguous displays.
	 * @param h				the height of the display in pixels
	 * @param w				the width of the display in pixels
	 * @return				the maximum fps
	 */
	public int getFPSLimit(int h, int w);
	
	/**
	 * Change a pixel color at the given x, y coordinates. Note that
	 * this method doesn't guarantee success.
	 * <p>
	 * The design details are left to the implementation to allow
	 * for things such as zig-zag displays, abstract shapes, and
	 * other forms of non-contiguous displays.
	 * @param x				pixel location on horizontal axis
	 * @param y				pixel location on vertical axis
	 * @param pix			color of the new pixel
	 */
	public void pushPixel(int x, int y, Color pix);
	
	/**
	 * Change a row of pixels to the desired colors. Note that 
	 * this method doesn't guarantee success.
	 * <p>
	 * The design details are left to the implementation to allow
	 * for things such as zig-zag displays, abstract shapes, and
	 * other forms of non-contiguous displays.
	 * @param y				row location on the vertical axis
	 * @param pixels		array of Colors to set
	 */
	public void pushHorizontalLine(int y, Color[] pixels);
	
	/**
	 * Change a column of pixels to the desired colors. Note that
	 * this method doesn't guarantee success.
	 * <p>
	 * The design details are left to the implementation to allow
	 * for things such as zig-zag displays, abstract shapes, and
	 * other forms of non-contiguous displays.
	 * @param x				column location on the horizontal axis
	 * @param pixels		array of Colors to set
	 */
	public void pushVerticalLine(int x, Color[] pixels);
	
	/**
	 * Change the entire array of pixels to the desired colors. Note
	 * that this method doesn't guarantee success.
	 * <p>
	 * The design details are left to the implementation to allow
	 * for things such as zig-zag displays, abstract shapes, and
	 * other forms of non-contiguous displays.
	 * @param pixels		two dimensional array of Colors to set
	 */
	public void pushFrame(Color[][] pixels);
}
