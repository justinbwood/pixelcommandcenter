package pcc.scraper;

import java.util.LinkedList;
import java.util.ServiceLoader;

import pcc.scraper.spi.ScraperHandler;

public class ScraperService {
	
	private static ScraperService service;
	private ServiceLoader<ScraperHandler> loader;
	
	private String defaultScraper;
	
	private ScraperService() {
		loader = ServiceLoader.load(ScraperHandler.class);
		defaultScraper = "";
	}
	
	public static synchronized ScraperService getInstance() {
		if (service == null) {
			service = new ScraperService();
		}
		return service;
	}
	
	/**
	 * Get a list of all registered scraper services.
	 * @return			a linked list of scraper names
	 */
	public LinkedList<String> getScraperList() {
		LinkedList<String> serviceList = new LinkedList<String>();
		for (ScraperHandler s : loader) {
			serviceList.add(s.getServiceName());
		}
		return serviceList;
	}
	
	/**
	 * Select a scraper to use as the default device. 
	 * @param sName				unique name of the scraper service
	 * @return					<b>true</b> if the service is available
	 */
	public boolean setDefaultScraper(String sName) {
		for (ScraperHandler s : loader) {
			if (s.getServiceName().equals(sName)) {
				defaultScraper = new String(sName);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Gets the default scraper service. Throws an exception if there is
	 * no default service, or the default service cannot be found.
	 * @return					the default ScraperHandler
	 * @throws Exception 		thrown if the default service is not registered
	 */
	public ScraperHandler getDefaultService() throws Exception {
		for (ScraperHandler s : loader) {
			if (s.getServiceName().equals(defaultScraper)) {
				return s;
			}
		}
		throw new Exception("No default scraper is registered.");
	}
	
}
