package pcc.plugin.spi;

public interface PluginHandler {
	/**
	 * Performs a set of actions when the plugin is loaded.
	 */
	public void load();
	
	/**
	 * Performs a set of actions when the plugin is unloaded.
	 */
	public void unload();
	
	/**
	 * Returns a String containing the name of the plugin.
	 * The plugin name must be unique so as to not collide
	 * with other PixelCommandCenter plugins.
	 * @return				the unique name of the plugin
	 */
	public String getServiceName();
}
